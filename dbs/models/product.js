var mongoose = require('mongoose');
var shop = require('./shop');

// Define our product schema
var ProductSchema = mongoose.Schema({
    shop: { type: mongoose.Schema.Types.ObjectId, ref: 'Shop' },
    serialNumber: { type: String, default: ''},
    name: { type: String, dafault: '' },
    type: { type: String, default: '' },
    brand: { type: String, default: '' },
    model: { type: String, default: '' },
    material: { type: String, default: '' },
    shortDescription: { type: String, dafault: '' },
    description: { type: String, dafault: '' },
    sku: { type: String, default: '' },
    quantity: { type: Number, default: 0 },
    weight: { type: Number, default: 0 },
    price: { type: String, default: '' },
    status: { type: String, enum: ['online', 'offline'] },
    portfolio: [{ type: String, default: '' }],
    picture: [{ 
        image: {
            Location: { type: String, default: '' }, 
            Key: { type: String, default: '' },
            Bucket: { type: String, default: '' }
        },
        order: { type: Number, default: 0 },
        isThumbnail: { type: Boolean, default: ''}
    }],
    products: [{
        name: { type: String, default: '' },
        quantity: { type: Number, default: 0 },
        price: { type: String, default: 0 }
    }],
    metaTitle: { type: String, dafault: '' },
    metaKeywords: { type: String, dafault: '' },
    metaDescription: { type: String, dafault: '' },
    search: [{ type: String, default:'' }],
    attribute: [{ type: String, default: '' }],
    tag: [{ type: String, default: '' }],
    createdAt: { type: Date, default: Date.now },
    updatedAt: { type: Date, default: null },
    deletedAt: { type: Date, default: null}
});

ProductSchema.plugin(require('./plugins/pagedFind'));
ProductSchema.index({ name: 1 });
ProductSchema.index({ serialNumber: 1 });
ProductSchema.set('autoIndex', process.env.NODE_ENV === 'development');

module.exports = mongoose.model('Product', ProductSchema);