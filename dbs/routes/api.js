var express = require('express');
// var passport = require('passport');
var router = express.Router();
var authenticated = require('../middleware/authenticated');
var ajaxed = require('../middleware/ajaxed');



// router.use(ajaxed);

// signup
// router.post('/signup', require('../controllers/signup'));
// router.post('/signup/social/', require('../controllers/signupSocial-alt'));

// router.get('/signup/facebook/', passport.authenticate('facebook', { callbackURL: 'http://localhost:3000/api/signup/facebook/callback',scope : 'email' }));
// router.get('/signup/facebook/callback/', require('../controllers/signup-f'));


// router.get('/signup/google/', passport.authenticate('google', { callbackURL: 'http://localhost:3000/api/signup/google/callback', scope: ['profile email'] }));
// router.get('/signup/google/callback/', require('../controllers/signup-g'));

// // login
// router.post('/login', require('../controllers/login'));
// // router.post('/login/forgot/', require('./controllers/forgot'));
// // router.post('/login/reset/:email/:token/', require('./controllers/reset'));
// // , require('../controllers/loginFacebook')
// router.get('/login/facebook/', passport.authenticate('facebook', { callbackURL: '/api/login/facebook/callback/' }));
// router.get('/login/facebook/callback/', require('../controllers/login-f'));
// router.get('/login/google/', passport.authenticate('google', { callbackURL: '/api/login/google/callback/', scope: ['profile email'] }));
// router.get('/login/google/callback/', require('../controllers/login-g'));

// // below need authenticated
// // router.use(authenticated);

// // logout
// router.get('/logout', require('../controllers/logout'));

// // validate
// router.get('/validate', require('../controllers/validate'));

// // admin > user
// router.post('/users', require('../controllers/postUsers'));
// router.get('/users', require('../controllers/getUsers'));
// router.get('/users/:id', require('../controllers/getUser'));
// router.put('/users/:id', require('../controllers/putUser'));

// router.delete('/users/:id', require('../controllers/deleteUser'));

// //bills
// //router.post('/bills', require('../controllers/postUsers'));
// router.get('/bills', require('../controllers/getBills'));
// router.get('/bills/:id', require('../controllers/getBill'));
// router.put('/bills/:id', require('../controllers/putBill'));


// router.get('/session', require('../controllers/session'));
// router.get('/item', require('../controllers/item'));

// //router.put('/bills/:id', require('../controllers/putUser'));
// //router.delete('/bills/:id', require('../controllers/deleteUser'));
// //bills

// // admin > account
// router.post('/accounts', require('../controllers/postAccounts'));
// router.get('/accounts', require('../controllers/getAccounts'));
// router.get('/accounts/:accountId', require('../controllers/getAccount'));
// router.put('/accounts/:userId', require('../controllers/putAccount'));
// router.delete('/accounts/:userId', require('../controllers/deleteAccount'));

// // router.post('/admins/:userId', require('../controllers/postAdmin'));
// // router.get('/admins', require('../controllers/getAdmins'));
// // router.get('/admins/:adminId', require('../controllers/getAdmin'));
// // router.put('/admins/:userId', require('../controllers/putAdmin'));
// // router.delete('/admins/:userId', require('../controllers/deleteAdmin'));

// // router.post('/managers', require('../controllers/postManagers'));
// // router.get('/managers', require('../controllers/getManagers'));
// // router.get('/managers/:id', require('../controllers/getManager'));
// // router.put('/managers/:id', require('../controllers/putManager'));
// // router.delete('/managers/:id', require('../controllers/deleteManager'));


// //paypal create order

// // router.get('/api/createorder', require('../models/paypal').createorder);
// // router.get('/api/orderExecute', require('../models/paypal').orderExecute);

// // products
// router.get('/products/:productId', require('../controllers/getProduct'));
// router.get('/products', require('../controllers/getProducts'));
// router.post('/products/:shopId', require('../controllers/postProduct'));
// router.put('/products/:productId', require('../controllers/putProduct'));
// router.delete('/products/:productId', require('../controllers/deleteProduct'));

// // shops
// router.get('/shops/:shopId', require('../controllers/getShop'));
// router.get('/shops', require('../controllers/getShops'));
// router.post('/shops', require('../controllers/postShop'));
// router.put('/shops/:shopId', require('../controllers/putShop'));
// router.delete('/shops/:shopId', require('../controllers/deleteShop'));
// // add delete ad in put api
// router.post('/ad', require('../controllers/postAd'));

// //category
// router.get('/categorys/:categoryId', require('../controllers/getCategory'));
// router.get('/categorys', require('../controllers/getCategorys'));
// router.post('/categorys', require('../controllers/postCategory'));
// router.put('/categorys/:categoryId', require('../controllers/putCategory'));
// router.delete('/categorys/:categoryId', require('../controllers/deleteCategory'));

// // portfolio
// router.get('/portfolios/:portfolioId', require('../controllers/getPortfolio'));
// router.get('/portfolios', require('../controllers/getPortfolios'));
// router.post('/portfolios', require('../controllers/postPortfolio'));
// router.put('/portfolios/:portfolioId', require('../controllers/putPortfolio'));
// router.delete('/portfolios/:portfolioId', require('../controllers/deletePortfolio'));

// // post
// router.get('/posts/:postId', require('../controllers/getPost'));
// router.get('/posts', require('../controllers/getPosts'));
// router.post('/posts', require('../controllers/postPost'));
// router.put('/posts/:postId', require('../controllers/putPost'));
// router.delete('/posts/:postId', require('../controllers/deletePost'));

// // images
// // add delete image in put api
// // router.delete('/image', require('../controllers/deleteImage'));
// router.post('/image', require('../controllers/postImage'));

router.get('/test', require('../controllers/test'));

module.exports = router;
