var express = require('express');
// var passport = require('passport');
var router = express.Router();
var authenticated = require('../middleware/authenticated');

router.get('/message', require('../controllers/getMessage'));

router.post('/message', require('../controllers/postMessage'));

module.exports = router;
