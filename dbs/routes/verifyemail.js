
var async = require('async');
var crypto = require('crypto');
var mongoose = require('mongoose');
var User = require('../models/user');
var EM = require('../models/email-dispatcher');
var express = require('express');
var router = express.Router();
var config = require('../config');


router.get('/forgot', function(req, res) {
  res.render('forgot', {
    user: req.user
  });
});

router.post('/api/forgot', function(req, res, next) {
  async.waterfall([
    function(done) {
      crypto.randomBytes(20, function(err, buf) {
        var token = buf.toString('hex');
        done(err, token);
      });
    },
    function(token, done) {
      User.findOne({ email: req.body.email }, function(err, user) {
        if (!user) {
          return res.redirect('/forgot');
        }

        user.resetPasswordToken = token;
        user.resetPasswordExpires = Date.now() + 3600000; // 1 hour

        user.save(function(err) {
          done(err, token, user);
        });
      });
    },
    function(token, user, done) {
      
      EM.dispatcherPWemail(req,token,user,function(err,o){
        done(err,'done');
      });
    }
  ], function(err) {
    if (err) return next(err);
    res.redirect('/forgot');
  });
});

router.get('/reset/:token', function(req, res) {
  User.findOne({ resetPasswordToken: req.params.token, resetPasswordExpires: { $gt: Date.now() } }, function(err, user) {
    if (!user) {
      return res.redirect('/forgot');
    }
    res.render('reset', {
      user: req.user
    });
  });
});


router.post('/reset/:token', function(req, res) {
  async.waterfall([
    function(done) {

      User.findOne({ resetPasswordToken: req.params.token, resetPasswordExpires: { $gt: Date.now() } }, function(err, user) {
        
        if (!user) {
          return res.redirect('/forgot');
        }

        User.encryptPassword(req.body.password, function(err, hash) {
        if (err) {
          return res.redirect('/forgot');
        }

        user.password = hash;
        user.resetPasswordToken = undefined;
        user.resetPasswordExpires = undefined;

        user.save(function(err) {
            done(err, user);
        });

        });
      });
    },
    function(user, done) {
      EM.successreset(req,user,function(err){
        done(err);
      });
      
    }
  ], function(err) {
    res.redirect('http://localhost:8080/login');
  });
});




router.get('/verify', function(req, res) {
  res.render('verify', {
    user: req.user
  });
});

router.post('/verify', function(req, res, next) {
  async.waterfall([
    function(done) {
      crypto.randomBytes(20, function(err, buf) {
        var token = buf.toString('hex');
        done(err, token);
      });
    },
    function(token, done) {
      User.findOne({ email: req.body.email }, function(err, user) {
        if (!user) {
          return res.redirect('/verify');
        }

        user.verifyemailToken = token;
        user.verifyemailExpires = Date.now() + 3600000; // 1 hour

        user.save(function(err) {
          done(err, token, user);
        });
      });
    },
    function(token, user, done) {
      EM.dispatcher(req,token, user ,function(err,o){
        done(err, 'done');
      });
    }
    // done(err, 'done');
  ], function(err) {
    if (err) return next(err);
    console.log('error dispatch email');
    res.redirect('/');
  });
});

router.get('/verifyemail/:token', function(req, res) {
User.findOne({ verifyemailToken: req.params.token, verifyemailExpires: { $gt: Date.now() } }, function(err, user) {
    if (!user) {
      return res.redirect('/verify');
    }

    User.findOne({ verifyemailToken: req.params.token, verifyemailExpires: { $gt: Date.now() } }, function(err, user) {
        if (!user) {
          return res.redirect('/verify');
        }


        user.verifyemailToken = undefined;
        user.verifyemailExpires = undefined;
        user.verifyemail = "true";

        user.save(function(err) {
      req.logIn(user, function(err) {
        console.log('email verify ok');
      res.redirect(config.shopcarturl);
      });
  });

        
      });

  });

});




module.exports = router;
