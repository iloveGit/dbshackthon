var express = require('express');
var passport = require('passport');
var router = express.Router();

// manager function
router.get('*', require('../middleware/authenticated'));
router.get('*', require('../middleware/admin'));
router.get('/helloworld', function (req, res) {
	return 'hello admin!!!!';
});
module.exports = router;