var express = require('express');
var passport = require('passport');
var router = express.Router();

// manager function
router.get('*', require('../middleware/authenticated'));
router.get('*', require('../middleware/account'));
router.get('/helloworld', function (req, res) {
	return 'hello account!!!!';
});
module.exports = router;