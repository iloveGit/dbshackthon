var express = require('express');
var router = express.Router();



router.get('/session', function(req, res) {

            res.locals.session = req.session;
            req.session.itemlist = req.session.itemlist  ? req.session.itemlist : [];
            data = req.session.itemlist;
            res.render('list', {itemlist:data});
});
router.post('/addsession', function(req, res) {

            res.locals.session = req.session;

            data = req.session.itemlist;
            data.push({name:req.body.name,
                       price:req.body.price,
                       quantity:req.body.quantity,
                       nport:req.body.quantity,
                       sku: req.body.portfolio,
                       propname: req.body.propname,
                        currency:"USD"});
            res.redirect('http://localhost:8080/');
});

router.post('/updatesession', function(req, res) {
    res.locals.session = req.session;
    req.session.itemlist = req.body.itemlist;
    res.send('ok');

});


router.post('/clear', function(req, res) {

            res.locals.session = req.session;
            req.session.itemlist = [];
            res.status(200);
});



router.post('/delsession', function(req, res) {
            index = req.body.index;

            res.locals.session = req.session;

            data = req.session.itemlist;
            console.log('delete the'+index);
            if (index > -1) {
                data.splice(index, 1);}
            res.redirect('http://localhost:8080/check');
});

router.get('/billlist', function(req, res) {

    Bill.find().exec(function(err, bills) {
    res.render('bill_list', { bills : bills, message: [{desc: "Order placed successfully.", type: "info"}]});
    });

});

var Bill = require('../models/bill');
router.get('/createbill', function(req, res) {

    var fieldsToSet = {
        payment_id: '9999',
        amount: 1333,
        description: 'pen',
        state: 'nopay'
    };

    Bill.create(fieldsToSet, function(err, user) {
        if (err) throw err;

        res.send('ok order checked');
    });



});




router.get('/updatebill/:billid', function(req, res) {
    res.locals.session = req.session;
    req.session.itemlist = req.session.itemlist  ? req.session.itemlist : [];

    data = req.session.itemlist ;

    Bill.findById(req.params.billid, function(err, bill) {
        if (err) throw err;


        bill.amount = 5566;
        bill.note = data;

        bill.save(function(err, updatedBill) {
            if (err) throw (err);
            res.send(updatedBill);
        });


    });

});


module.exports = router;