var config = require('../config');
var request = require('request');

function parseTextMessage (recipientId, messageText) {
	if (/(hi)/i.test(messageText) || /(嗨)/i.test(messageText)) {
		var special = false;
		var messageData = {
              recipient: {
                id: recipientId
              },
              message: {
                attachment: {
                  type: "template",
                  payload: {
                    template_type: "button",
                    text: "您想要什麼樣的服務？",
                    buttons: [{
                      title: "個人金融",
                      type: "postback",
                      payload: "personal_finance"
                    }, {
                      title: "企業金融",
                      type: "postback",
                      payload: "bussiness_finance"
                    }, {
                      title: "星展最新消息",
                      type: "postback",
                      payload: "dbs_news"
                    }]
                  }
                }
              }
            };
	} else if (/(investment)/.test(messageText) || /(投資)/.test(messageText)) {
		var messageData = {
              recipient: {
                id: recipientId
              },
              message: {
                attachment: {
                  type: "template",
                  payload: {
                    template_type: "button",
                    text: "那可以先請問你的風險屬性嗎？",
                    buttons: [{
                      title: "積極型",
                      type: "postback",
                      payload: "aggresive"
                    }, {
                      title: "保守型",
                      type: "postback",
                      payload: "normal"
                    }, {
                      title: "穩健型",
                      type: "postback",
                      payload: "health"
                    }]
                  }
                }
              }
            };
	} else if (/(積極型)/.test(messageText)) {
		var messageData = {
              recipient: {
                id: recipientId
              },
              message: {
                attachment: {
                  type: "template",
                  payload: {
                    template_type: "generic",
                    elements: [{
                        title: "積極型投資組合",
                        item_url: "http://www.dbs.com.tw/treasures-zh/investments/asset-allocation/aggressive-portfolio",
                        image_url: "http://www.dbs.com.tw/iwov-resources/images/investments/tw-treasures/05_Aggressive-Detail-tw-zh.PNG"
                    }]
                  }
                }
              }
            };
	} else if (/(川普)/.test(messageText)) {
		var messageData = {
              recipient: {
                id: recipientId
              },
              message: {
                attachment: {
                  type: "template",
                  payload: {
                  	template_type: "generic",
                    elements: [{
                        title: "川普研究報告",
                        item_url: "http://www.dbs.com.tw/treasures-zh/investments/asset-allocation/aggressive-portfolio",
                        image_url: "https://s3-ap-northeast-1.amazonaws.com/bonweb-assets/images/trump.jpg",
                        buttons: [{
                          title: "連結",
                          type: "postback",
                          payload: "link"
                        }]
                    }]
                  }
                }
              }
            };
	} else if (/(黃金)/.test(messageText)) {
		var messageData = {
              recipient: {
                id: recipientId
              },
              message: {
                attachment: {
                  type: "template",
                  payload: {
                  	template_type: "generic",
                    elements: [{
                        title: "黃金走勢圖",
                        item_url: "http://www.dbs.com.tw/treasures-zh/investments/asset-allocation/aggressive-portfolio",
                        image_url: "https://s3-ap-northeast-1.amazonaws.com/bonweb-assets/images/IMG_0843.JPG",
                        buttons: [{
                          title: "連結",
                          type: "postback",
                          payload: "link"
                        }]
                    }]
                  }
                }
              }
            };
	} else if (/(基金)/.test(messageText)) {
		var messageData = {
              recipient: {
                id: recipientId
              },
              message: {
                attachment: {
                  type: "template",
                  payload: {
                    template_type: "button",
                    text: "這邊找到三個基金",
                    buttons: [{
                      title: "貝萊德世界金融A2-EUR 2016/11/17 基金淨值19.02",
                      type: "web_url",
                      url: "https://internet-banking.dbs.com.tw/tw/retail/logon"
                    }, {
                      title: "柏瑞日本小型公司股票A3 2016/11/17 基金淨值5304.55",
                      type: "web_url",
                      url: "https://internet-banking.dbs.com.tw/tw/retail/logon"
                    }, {
                      title: "聯博-日本策略價值AD月配紐幣避險級別(基金之配息來源可能為本金) 2016/11/17 基金淨值15.95",
                      type: "web_url",
                      url: "https://internet-banking.dbs.com.tw/tw/retail/logon"
                    }]
                  }
                }
              }
            };
            special = true;
	} else {
		var messageData = {
              recipient: {
                id: recipientId
              },
              message: {
                attachment: {
                  type: "template",
                  payload: {
                    template_type: "button",
                    text: "我們聽不懂您說的話, 如果有任何問題歡迎跟我們聯繫！",
                    buttons: [{
                       "type":"phone_number",
                       "title":"Call",
                       "payload":"+8860266129889"
                    }]
                  }
                }
              }
            };
	}
	callSendAPI(messageData, special);
}

function callSendAction (recipientId, action) {
  request({
    uri: 'https://graph.facebook.com/v2.6/me/messages?access_token=' + config.PAGE_ACCESS_TOKEN,
    method: 'POST',
    json: {
      "recipient":{
  	    "id": recipientId
      },
      "sender_action": action
    }
  }, function (error, response, body) {
    // if (!error && response.statusCode == 200) {
    // } else {
    // }
  });
}

function callSendAPI(messageData, special) {
  callSendAction(messageData.recipient.id, "typing_on")
  request({
    uri: 'https://graph.facebook.com/v2.6/me/messages',
    qs: { access_token: config.PAGE_ACCESS_TOKEN },
    method: 'POST',
    json: messageData
  }, function (error, response, body) {
    if (!error && response.statusCode == 200) {
      var recipientId = body.recipient_id;
      var messageId = body.message_id;

      console.log("Successfully sent generic message with id %s to recipient %s", 
        messageId, recipientId);
    } else {
      console.log(error);
    }
    if (special) {
    	var sMessageData = {
              recipient: {
                id: recipientId
              },
              message: {
                attachment: {
                  type: "template",
                  payload: {
                    template_type: "button",
                    text: "我們偵測到您來自新加坡！請問在台灣旅費還足夠嗎？有需要使用換匯服務嗎？",
                    buttons: [{
                       "type":"postback",
                       "title":"要",
                       "payload":"要"
                    }, {
                       "type":"postback",
                       "title":"不要",
                       "payload":"不要"
                    }]
                  }
                }
              }
            };
        setTimeout(function () {
        	callSendAPI(sMessageData, false);
        }, 20000);
    }
    callSendAction(messageData.recipient.id, "typing_off");
  });
}

function receivedPostback(event) {
  var senderID = event.sender.id;
  var recipientID = event.recipient.id;
  var timeOfPostback = event.timestamp;
  var payload = event.postback.payload;

  console.log("Received postback for user %d and page %d with payload '%s' " + 
    "at %d", senderID, recipientID, payload, timeOfPostback);

  sendTextMessage(senderID, "Postback called");
}

function sendTextMessage(recipientId, messageText) {
  var messageData = {
    recipient: {
      id: recipientId
    },
    message: {
      text: messageText
    }
  };

  callSendAPI(messageData);
}

function sendGenericMessage(recipientId, messageText) {
  var messageData = {
    recipient: {
      id: recipientId
    },
    message: {
      attachment: {
        type: "template",
        payload: {
          template_type: "generic",
          elements: [{
            title: "rift",
            subtitle: "Next-generation virtual reality",
            item_url: "https://www.oculus.com/en-us/rift/",               
            image_url: "http://messengerdemo.parseapp.com/img/rift.png",
            buttons: [{
              type: "web_url",
              url: "https://www.oculus.com/en-us/rift/",
              title: "Open Web URL"
            }, {
              type: "postback",
              title: "Call Postback",
              payload: "Payload for first bubble",
            }],
          }, {
            title: "touch",
            subtitle: "Your Hands, Now in VR",
            item_url: "https://www.oculus.com/en-us/touch/",               
            image_url: "http://messengerdemo.parseapp.com/img/touch.png",
            buttons: [{
              type: "web_url",
              url: "https://www.oculus.com/en-us/touch/",
              title: "Open Web URL"
            }, {
              type: "postback",
              title: "Call Postback",
              payload: "Payload for second bubble",
            }]
          }]
        }
      }
    }
  };

  callSendAPI(messageData);
}

function receivedMessage(event) {
  var senderID = event.sender.id;
  var recipientID = event.recipient.id;
  var timeOfMessage = event.timestamp;
  var message = event.message;

  console.log("Received message for user %d and page %d at %d with message", 
    senderID, recipientID, timeOfMessage);

  var messageId = message.mid;
  var messageText = message.text;
  var messageAttachments = message.attachments;

  if (messageText) {
    // switch (messageText) {
    //   case 'generic':
    //     sendGenericMessage(senderID);
    //     break;
    //   default:
    //     sendTextMessage(senderID, messageText);
    // }
    parseTextMessage(senderID, messageText);
  } else if (messageAttachments) {
    sendTextMessage(senderID, "Message with attachment received");
  }
}

module.exports = function (req, res) {
  var data = req.body;

  if (data.object === 'page') {

    data.entry.forEach(function(entry) {
      var pageID = entry.id;
      var timeOfEvent = entry.time;

      entry.messaging.forEach(function(event) {
        if (event.message) {
          receivedMessage(event);
        } else {
          console.log("Webhook received unknown event: ", event);
        }
      });
    });

    res.sendStatus(200);
  }
}