module.exports = function(req, res) {
	var workflow = require('../util/workflow')(req, res);


	workflow.on('echo', function () {
		workflow.outcome.string = 'You Got It !!';
		workflow.emit('response');
	});

	workflow.emit('echo');
}