var app = require('../app');
var config = require('../config');
var request = require('supertest');
var superagent = require('superagent');
var agent = superagent.agent();

function random() {
	var text = "";
	var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

	for (var i = 0; i < 5; i++)
		text += possible.charAt(Math.floor(Math.random() * possible.length));

	return text;
}

var user_id = '';
var rand_username = random();
var rand_password = random();
var rand_email = random() + '@' + random() + '.com';

var user_id1 = '';
var rand_username1 = random();
var rand_password1 = random();
var rand_email1 = random() + '@' + random() + '.com';

var user_id2 = random();
var rand_username2 = random();
var rand_password2 = random();
var rand_email2 = random() + '@' + random() + '.com';

describe('Test Create(POST) of Users: /api/users', function() {
	it('Without any data /api/users', function(done) {
		request(app)
			.post('/api/users')
			.expect('Content-Type', 'application/json; charset=utf-8')
			.expect(400)
			.end(function(err, data) {
				if (err) {
					return done(err);
				}
				if (data.body.success != false) {
					console.log(data.body);
					return done('success should be false');
				}
				if (data.body.errfor.username != 'required') {
					console.log(data.body);
					return done('errfor.username should be required');
				}
				if (data.body.errfor.password != 'required') {
					console.log(data.body);
					return done('errfor.password should be required');
				}
				if (data.body.errfor.email != 'required') {
					console.log(data.body);
					return done('errfor.email should be required');
				}
				return done();
			});
	});

	it('Without username data /api/users', function(done) {
		request(app)
			.post('/api/users')
			.expect('Content-Type', 'application/json; charset=utf-8')
			.send({
				'password': rand_password,
				'email': rand_email
			})
			.expect(400)
			.end(function(err, data) {
				if (err) {
					return done(err);
				}
				if (data.body.success != false) {
					console.log(data.body);
					return done('success should be false');
				}
				if (data.body.errfor.username != 'required') {
					console.log(data.body);
					return done('errfor.username should be required');
				}
				if (data.body.errfor.password == 'required') {
					console.log(data.body);
					return done('errfor.password should be required');
				}
				if (data.body.errfor.email == 'required') {
					console.log(data.body);
					return done('errfor.email should be required');
				}
				return done();
			});
	});

	it('Without password data /api/users', function(done) {
		request(app)
			.post('/api/users')
			.expect('Content-Type', 'application/json; charset=utf-8')
			.send({
				'username': rand_username,
				'email': rand_email
			})
			.expect(400)
			.end(function(err, data) {
				if (err) {
					return done(err);
				}
				if (data.body.success != false) {
					console.log(data.body);
					return done('success should be false');
				}
				if (data.body.errfor.username == 'required') {
					console.log(data.body);
					return done('errfor.username should be required');
				}
				if (data.body.errfor.password != 'required') {
					console.log(data.body);
					return done('errfor.password should be required');
				}
				if (data.body.errfor.email == 'required') {
					console.log(data.body);
					return done('errfor.email should be required');
				}
				return done();
			});
	});

	it('Without email data /api/users', function(done) {
		request(app)
			.post('/api/users')
			.expect('Content-Type', 'application/json; charset=utf-8')
			.send({
				'username': rand_username,
				'password': rand_password,
			})
			.expect(400)
			.end(function(err, data) {
				if (err) {
					return done(err);
				}
				if (data.body.success != false) {
					console.log(data.body);
					return done('success should be false');
				}
				if (data.body.errfor.username == 'required') {
					console.log(data.body);
					return done('errfor.username should be required');
				}
				if (data.body.errfor.password == 'required') {
					console.log(data.body);
					return done('errfor.password should be required');
				}
				if (data.body.errfor.email != 'required') {
					console.log(data.body);
					return done('errfor.email should be required');
				}
				return done();
			});
	});

	it('Create User /api/users', function(done) {
		request(app)
			.post('/api/users')
			.expect('Content-Type', 'application/json; charset=utf-8')
			.send({
				'username': rand_username,
				'password': rand_password,
				'email': rand_email
			})
			.expect(200)
			.end(function(err, data) {
				if (err) {
					return done(err);
				}
				if (data.body.success != true) {
					console.log(data.body);
					return done('success should be true');
				}
				user_id = data.body.user._id;
				console.log('user_id: ' + user_id);
				return done();
			});
	});

	it('Create User /api/users', function(done) {
		request(app)
			.post('/api/users')
			.expect('Content-Type', 'application/json; charset=utf-8')
			.send({
				'username': rand_username1,
				'password': rand_password1,
				'email': rand_email1
			})
			.expect(200)
			.end(function(err, data) {
				if (err) {
					return done(err);
				}
				if (data.body.success != true) {
					console.log(data.body);
					return done('success should be true');
				}
				user_id1 = data.body.user._id;
				console.log('user_id: ' + user_id1);
				return done();
			});
	});

	it('Duplicate username /api/users', function(done) {
		request(app)
			.post('/api/users')
			.expect('Content-Type', 'application/json; charset=utf-8')
			.send({
				'username': rand_username,
				'password': rand_password,
				'email': rand_email
			})
			.expect(400)
			.end(function(err, data) {
				if (err) {
					return done(err);
				}
				if (data.body.errfor.username != 'duplicate') {
					console.log(data.body);
					return done('errfor.username should be duplicate');
				};
				return done();
			});
	});

	it('Duplicate email /api/users', function(done) {
		request(app)
			.post('/api/users')
			.expect('Content-Type', 'application/json; charset=utf-8')
			.send({
				'username': random(),
				'password': rand_password,
				'email': rand_email
			})
			.expect(400)
			.end(function(err, data) {
				if (err) {
					return done(err);
				}
				if (data.body.errfor.email != 'duplicate') {
					console.log(data.body);
					return done('errfor.email should be duplicate');
				};
				return done();
			});
	});
});

describe('Test Read(GET) one of Users: /api/user/:id', function() {
	it('Without any data /api/users/' + user_id, function(done) {
		request(app)
			.get('/api/users/' + user_id)
			.expect('Content-Type', 'application/json; charset=utf-8')
			.expect(200)
			.end(function(err, data) {
				if (err) {
					return done(err);
				}
				if (data.body.success != true) {
					console.log(data.body);
					return done('success should be true');
				}
				return done();
			});
	});

	it('Does not exist user /api/users/' + user_id2, function(done) {
		request(app)
			.get('/api/users/' + user_id2)
			.expect('Content-Type', 'application/json; charset=utf-8')
			.expect(404)
			.end(function(err, data) {
				if (err) {
					return done(err);
				}
				if (data.body.success != false) {
					console.log(data.body);
					return done('success should be false');
				}
				return done();
			});
	});

});

describe('Test Read(GET)of Users: /api/users/', function() {
	it('Without any data /api/users/', function(done) {
		request(app)
			.get('/api/users/')
			.expect('Content-Type', 'application/json; charset=utf-8')
			.expect(200)
			.end(function(err, data) {
				if (err) {
					return done(err);
				}
				if (data.body.success != true) {
					console.log(data.body);
					return done('success should be true');
				}
				return done();
			});
	});
});

describe('Test Update(PUT) of Users: /api/users/:id', function() {
	it('Without any data /api/users/' + user_id, function(done) {
		request(app)
			.put('/api/users/' + user_id)
			.expect('Content-Type', 'application/json; charset=utf-8')
			.expect(400)
			.end(function(err, data) {
				if (err) {
					return done(err);
				}
				if (data.body.success != false) {
					console.log(data.body);
					return done('success should be false');
				}
				if (data.body.errfor.username != 'required') {
					console.log(data.body);
					return done('errfor.username should be required');
				}
				if (data.body.errfor.email != 'required') {
					console.log(data.body);
					return done('errfor.email should be required');
				}
				return done();
			});
	});

	it('Without username data /api/users' + user_id, function(done) {
		request(app)
			.put('/api/users/' + user_id)
			.expect('Content-Type', 'application/json; charset=utf-8')
			.send({
				'email': rand_email
			})
			.expect(400)
			.end(function(err, data) {
				if (err) {
					return done(err);
				}
				if (data.body.success != false) {
					console.log(data.body);
					return done('success should be false');
				}
				if (data.body.errfor.username != 'required') {
					console.log(data.body);
					return done('errfor.username should be required');
				}
				if (data.body.errfor.email == 'required') {
					console.log(data.body);
					return done('errfor.email should be required');
				}
				return done();
			});
	});

	it('Without email data /api/users', function(done) {
		request(app)
			.put('/api/users/' + user_id)
			.expect('Content-Type', 'application/json; charset=utf-8')
			.send({
				'username': rand_username,
			})
			.expect(400)
			.end(function(err, data) {
				if (err) {
					return done(err);
				}
				if (data.body.success != false) {
					console.log(data.body);
					return done('success should be false');
				}
				if (data.body.errfor.username == 'required') {
					console.log(data.body);
					return done('errfor.username should be required');
				}
				if (data.body.errfor.email != 'required') {
					console.log(data.body);
					return done('errfor.email should be required');
				}
				return done();
			});
	});

	it('Update User /api/users' + user_id, function(done) {
		request(app)
			.put('/api/users/' + user_id)
			.expect('Content-Type', 'application/json; charset=utf-8')
			.send({
				'username': rand_username2,
				'email': rand_email2,
				'isActive': 'yes'
			})
			.expect(200)
			.end(function(err, data) {
				if (err) {
					return done(err);
				}

				if (data.body.success != true) {
					console.log(data.body);
					return done('success should be true');
				}
				var user = data.body.user;

				if (user.username != rand_username2 && user.email != rand_email2 && user.isActive != 'yes') {
					console.log(rand_email2)
					console.log(data.body);
					return done('update data is not match');
				}
				return done();
			});
	});

	it('Duplicate username /api/users' + user_id, function(done) {
		request(app)
			.put('/api/users/' + user_id)
			.expect('Content-Type', 'application/json; charset=utf-8')
			.send({
				'username': rand_username1,
				'email': rand_email
			})
			.expect(400)
			.end(function(err, data) {
				if (err) {
					return done(err);
				}
				if (data.body.errfor.username != 'duplicate') {
					console.log(data.body);
					return done('errfor.username should be duplicate');
				};
				return done();
			});
	});

	it('Duplicate email /api/users' + user_id, function(done) {
		request(app)
			.put('/api/users/' + user_id)
			.expect('Content-Type', 'application/json; charset=utf-8')
			.send({
				'username': rand_username,
				'email': rand_email1
			})
			.expect(400)
			.end(function(err, data) {
				if (err) {
					return done(err);
				}
				if (data.body.errfor.email != 'duplicate') {
					console.log(data.body);
					return done('errfor.email should be duplicate');
				};
				return done();
			});
	});

	it('Does not exist user /api/users/' + user_id2, function(done) {
		request(app)
			.put('/api/users/' + user_id2)
			.expect('Content-Type', 'application/json; charset=utf-8')
			.send({
				'username': rand_username,
				'email': rand_email
			})
			.expect(404)
			.end(function(err, data) {
				if (err) {
					return done(err);
				}
				if (data.body.success != false) {
					console.log(data.body);
					return done('success should be false');
				}
				return done();
			});
	});
});

describe('Test Delete(DELETE)of Users: /api/users/:id', function() {
	it('Without login admin /api/users/' + user_id, function(done) {
		request(app)
			.delete('/api/users/' + user_id)
			.expect('Content-Type', 'application/json; charset=utf-8')
			.expect(401)
			.end(function(err, data) {
				if (err) {
					return done(err);
				}
				if (data.body.success != false) {
					console.log(data.body);
					return done('success should be false');
				}
				return done();
			});
	});

	it('login admin /api/login/', function(done) {
			request(app)
			.post('/api/login/')
			.expect('Content-Type', 'application/json; charset=utf-8')
			.expect(200)
			.send({
				username: 'root',
				password: 'asdf1234'
			})
			.end(function(err, data) {
				if (err) {
					return done(err);
				}
				if (data.body.success != true) {
					console.log(data.body);
					return done('success should be true');
				}
				agent.saveCookies(res);
				return done();
			});
	});

	it('login admin with delete user /api/users/' + user_id, function(done) {
		request(app)
			.delete('/api/users/' + user_id)
			.expect('Content-Type', 'application/json; charset=utf-8')
			.expect(200)
			.end(function(err, data) {
				if (err) {
					return done(err);
				}
				if (data.body.success != false) {
					console.log(data.body);
					return done('success should be false');
				}
				return done();
			});
	});
});