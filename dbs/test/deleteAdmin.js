var mongoose = require('mongoose');
var config = require('../config');
var AdminGroup = require('../models/adminGroup');
var Admin = require('../models/admin');
var User = require('../models/user');
var event = new(require('events').EventEmitter)();

var deleteAdmin = function() {
	mongoose.connect(config.mongodb.uri);
	event.on('deleteAdminGroup', function() {
		console.log('Delete Admin Group');

		AdminGroup.findOneAndRemove({
			_id: 'root',
			name: 'Root'
		}, function(err) {
			if (err) {
				console.log(err);
				return event.emit('finish');
			}

			event.emit('deleteAdmin');
		});
	});

	event.on('deleteAdmin', function() {
		console.log('Delete Admin');

		Admin.findOneAndRemove({
			"name.first": 'Root'
		}, function(err, admin) {
			if (err) {
				console.log(err);
				return event.emit('finish');
			}

			event.emit('deleteUser');
		});
	});

	event.on('deleteUser', function() {
		var fieldsToSet = {
			username: 'root',
			email: 'trylovetom@gmail.com'
		};

		console.log('Delete User: root trylovetom@gmail.com');
		User.findOneAndRemove(fieldsToSet, function(err, user) {
			if (err) {
				console.log(err);
				return event.emit('finish');
			}
			event.emit('finish');
		});
	});

	event.on('finish', function() {
		console.log('finish delete admin');
		mongoose.connection.close();
	});

	event.emit('deleteAdminGroup');
}

deleteAdmin();