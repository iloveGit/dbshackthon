var mongoose = require('mongoose');
var config = require('../config');
var AdminGroup = require('../models/adminGroup');
var Admin = require('../models/admin');
var User = require('../models/user');
var event = new(require('events').EventEmitter)();

var makeAdmin = function() {
	mongoose.connect(config.mongodb.uri);
	event.on('createAdminGroup', function() {
		console.log('Create Admin Group');

		AdminGroup.create({
			_id: 'root',
			name: 'Root'
		}, function(err, adminGroup) {
			if (err) {
				console.log(err);
				return event.emit('finish');
			}

			event.adminGroup = adminGroup;
			event.emit('createAdmin');
		});
	});

	event.on('createAdmin', function() {
		console.log('Create Admin');

		Admin.create({
			name: {
				first: 'Root',
				last: 'Admin',
				full: 'Root Admin'
			},
			groups: ['root']
		}, function(err, admin) {
			if (err) {
				console.log(err);
				return event.emit('finish');
			}

			event.admin = admin;
			event.emit('createUser');
		});
	});

	event.on('createUser', function() {
		console.log('Encrypt Password: asdf1234');
		User.encryptPassword('asdf1234', function(err, hash) {
			if (err) {
				console.log(err);
				return event.emit('finish');
			}

			var fieldsToSet = {
				isActive: 'yes',
				username: 'root',
				email: 'trylovetom@gmail.com',
				password: hash,
				search: [
					'root',
					'trylovetom@gmail.com'
				],
				roles: {
					admin: event.admin._id
				}
			};

			console.log('Create User: root trylovetom@gmail.com');
			User.create(fieldsToSet, function(err, user) {
				if (err) {
					console.log(err);
					return event.emit('finish');
				}

				event.user = user;
				event.emit('patchAdmin');
			});
		});
	});

	event.on('patchAdmin', function() {
		console.log('Patch Admin');
		event.admin.update({
			'user.id': event.user._id,
			'user.name': event.user.username
		}, function(err, admin) {
			if (err) {
				console.log(err);
				return event.emit('finish');
			}

			event.admin = admin;
			return event.emit('finish');
		});
	});

	event.on('finish', function() {
		console.log('finish make admin');
		mongoose.connection.close();
	});

	event.emit('createAdminGroup');
}

var deleteAdmin = function() {
	mongoose.connect(config.mongodb.uri);
	event.on('deleteAdminGroup', function() {
		console.log('Delete Admin Group');

		AdminGroup.findOneAndRemove({
			_id: 'root',
			name: 'Root'
		}, function(err) {
			if (err) {
				console.log(err);
				return event.emit('finish');
			}

			event.emit('deleteAdmin');
		});
	});

	event.on('deleteAdmin', function() {
		console.log('Delete Admin');

		Admin.findOneAndRemove({
			name: {
				first: 'Root',
				last: 'Admin',
				full: 'Root Admin'
			},
			groups: ['root']
		}, function(err, admin) {
			if (err) {
				console.log(err);
				return event.emit('finish');
			}

			event.emit('deleteUser');
		});
	});

	event.on('deleteUser', function() {
		var fieldsToSet = {
			username: 'root',
			email: 'trylovetom@gmail.com'
		};

		console.log('Delete User: root trylovetom@gmail.com');
		User.findOneAndRemove(fieldsToSet, function(err, user) {
			if (err) {
				console.log(err);
				return event.emit('finish');
			}
			event.emit('finish');
		});
	});

	event.on('finish', function() {
		console.log('finish delete admin');
		mongoose.connection.close();
	});

	event.emit('deleteAdminGroup');
}

makeAdmin();