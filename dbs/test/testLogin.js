var request = require('supertest');
// var should = require('should');
var app = require('../app');
 
var Cookies;
 
describe('Functional Test <Sessions>:', function () {
  it('should create user session for valid user', function (done) {
    request(app)
      .post('/api/login')
      .set('Accept','application/json')
      .send({"username": "root", "password": "asdf1234"})
      .expect('Content-Type', /json/)
      .expect(200)
      .end(function (err, res) {
        // res.body.id.should.equal('1');
        // res.body.short_name.should.equal('Test user');
        // res.body.email.should.equal('user_test@example.com');
        // Save the cookie to use it later to retrieve the session
        console.log(res.body);
        Cookies = res.headers['set-cookie'].pop().split(';')[0];
        done();
      });
  });
  it('should get user session for current user', function (done) {
    var req = request(app).get('/api/validate');
    // Set cookie to get saved user session
    req.cookies = Cookies;
    req.set('Accept','application/json')
      .expect('Content-Type', /json/)
      .expect(200)
      .end(function (err, res) {
        // res.body.id.should.equal('1');
        // res.body.short_name.should.equal('Test user');
        // res.body.email.should.equal('user_test@example.com');
        console.log(res.body);
        done();
      });
  });
});