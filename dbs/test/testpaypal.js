var app = require('../app');
//var mongoose = require('mongoose');
var config = require('../config');
var request = require('supertest');

// test helloworld
describe('Test payapl router /orderConfirm', function() {
    /*before(function (done) {
        if (mongoose.connection.db) {
            done();
        }

        mongoose.connect(config.mongodb.url, done);
    });*/

    it('orderConfirm', function(done) {
        request(app)
            .get('/orderConfirm')
            .expect(200)
            .end(function(err, data) {
                if (err) {
                    done(err);
                }
                console.log(data.body);
                done();
            });
    });

    it('orderCreate', function(done) {
        request(app)
            .post('/api/createorder')
            .field('amount', 100)
            .field('desc', "bot")
            .expect(400)
            .end(function(err, data) {
                if (err) {
                    done(err);
                }
                console.log(data.body);
                done();
            });
    });
});



describe('Test router /verify', function() {
    before(function() {
        // 任何需要在測試前執行的程式
    });
    after(function() {
        // 任何需要在測試後刪除的資料
    });

    it('verify user email', function(done) {
        request(app)
            .get('/verify')
            .expect(200)
            .end(function(err, data) {
                if (err) {
                    done(err);
                }
                console.log(data.body);
                done();
            });
    });

});


describe('Test router /forgot', function() {
    before(function() {
        // 任何需要在測試前執行的程式
    });
    after(function() {
        // 任何需要在測試後刪除的資料
    });

    it('send reset pw email & token', function(done) {
        request(app)
            .get('/forgot')
            .expect(200)
            .end(function(err, data) {
                if (err) {
                    done(err);
                }
                console.log(data.body);
                done();
            });
    });

});
