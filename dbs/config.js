exports.port = process.env.PORT || 443;
exports.projectName = 'dbshackthon';
exports.mongodb = {
	uri: process.env.MONGOLAB_URI || process.env.MONGOHQ_URL || 'mongodb://52.198.104.127:27017/' + exports.projectName
};
exports.allowedOrigins = ['http://localhost:443', 'http://localhost:8080'];
exports.cryptoKey = 'k3yb0ardc4t';
exports.requireAccountVerification = false;
exports.ssl = {
	basePath: '/etc/letsencrypt/live/dbshackthon.ptrgl.com/', 
	chainPath: 'chain.pem',
	fullchainPath: 'fullchain.pem',
	privkeyPath: 'privkey.pem',
	certPath: 'cert.pem'
}
exports.auth = 'dbshackthon'
exports.PAGE_ACCESS_TOKEN = 'EAAE22kJIgYkBAHxStG2KmYBhZARF6SKJoR1EZCZBRYRZA1ZArCfvKIsoR2m6fb2kSzuvNHsdtWEKZCaU1ZC0r2pFHZACPGkVgTOjrY6tIqGA41vZANdJe1uPCzwNOOB4jG8hdygXUnyPQC8ekceHAA7rpeZCg85FcdrcfEZBwVjGrC5uAZDZD'