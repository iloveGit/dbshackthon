var User = require('../models/user')

var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;
var FacebookStrategy = require('passport-facebook').Strategy;
var GoogleStrategy = require('passport-google-oauth').OAuth2Strategy;

var config = require('../config');

passport.use(new LocalStrategy(function(username, password, done) {
  var conditions = {
    isActive: 'yes'
  };

  if (username.indexOf('@') === -1) {
    conditions.username = username;
  } else {
    conditions.email = username.toLowerCase();
  }

  User.findOne(conditions, function(err, user) {
    if (err) {
      return done(err);
    }

    if (!user) {
      return done(null, false, {
        message: 'Unknown user'
      });
    }

    User.validatePassword(password, user.password, function(err, isValid) {
      if (err) {
        return done(err);
      }

      if (!isValid) {
        return done(null, false, {
          message: 'Invalid password'
        });
      }

      return done(null, user);
    });
  });
}));

if (config.oauth.facebook.id) {
  passport.use(new FacebookStrategy({
      clientID: config.oauth.facebook.id,
      clientSecret: config.oauth.facebook.secret
    },
    function(accessToken, refreshToken, profile, done) {
      done(null, false, {
        accessToken: accessToken,
        refreshToken: refreshToken,
        profile: profile
      });
    }
  ));
}

if (config.oauth.google.id) {
  passport.use(new GoogleStrategy({
      clientID: config.oauth.google.id,
      clientSecret: config.oauth.google.secret
    },
    function(accessToken, refreshToken, profile, done) {
      done(null, false, {
        accessToken: accessToken,
        refreshToken: refreshToken,
        profile: profile
      });
    }
  ));
}

passport.serializeUser(function(user, done) {
  done(null, user._id);
});

passport.deserializeUser(function(id, done) {
  User.findOne({
    _id: id
  }).populate('roles.admin').populate('roles.manager').populate('roles.account').exec(function(err, user) {
    if (user && user.roles && user.roles.admin) {
      user.roles.admin.populate("groups", function(err, admin) {
        done(err, user);
      });
    } else if (user && user.roles && user.roles.manager) {
      user.roles.manager.populate("groups", function(err, manager) {
        done(err, user);
      });
    } else {
      done(err, user);
    }
  });
});
