exports = module.exports = function(req, res) {
  var workflow = new(require('events').EventEmitter)();

  workflow.outcome = {
    success: false,
    errors: [],
    errfor: {},
  };

  workflow.status = null;

  workflow.hasErrors = function() {
    return Object.keys(workflow.outcome.errfor).length !== 0 || workflow.outcome.errors.length !== 0;
  };

  workflow.on('exception', function(err) {
    workflow.status = 500;
    workflow.outcome.errors.push('Exception: ' + err);
    return workflow.emit('response');
  });

  workflow.on('response', function() {
    workflow.outcome.success = !workflow.hasErrors();
    
    if (workflow.hasErrors() && !workflow.status) {
      workflow.status = 400;
    }
    
    if (!workflow.status) {
      res.send(workflow.outcome);
    } else {
      res.status(workflow.status).send(workflow.outcome);
    }
  });

  return workflow;
};