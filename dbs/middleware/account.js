var config = require('../config');

module.exports = function (req, res, next) {
    if ((req.user) && req.user.canPlayRoleOf('account')) {
        if (config.requireAccountVerification) {
            /*if (req.user.roles.account.isVerified !== 'yes' && !/^\/account\/verification\//.test(req.url)) {
                return res.redirect('/account/verification/');
            }*/
        }
        return next();
    }
    return res.redirect(config.homeUrl);
}