module.exports = function(req, res, next) {
	if (!req.xhr) {
    	return res.status(400).send('Somethig wrong happened!');
    }
    return next();
}