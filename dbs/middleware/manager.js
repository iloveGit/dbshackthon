module.exports = function (req, res, next) {
    if ((req.user) && req.user.canPlayRoleOf('manager')) {
        return next();
    }
    return res.redirect(config.homeUrl);
}