module.exports = function (req, res, next) {
    if ((req.user) && req.user.canPlayRoleOf('admin')) {
        return next();
    }
    return res.status(401).redirect(config.homeUrl);
}