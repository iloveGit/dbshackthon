module.exports = function(app, passport) {

	    app.get('/', function(req, res) {
        res.send('index');
    });

    // PROFILE SECTION =========================
    app.get('/profile', function(req, res) {
        res.send('profile');
    });

    // LOGOUT ==============================
    app.get('/logout', function(req, res) {
        req.logout();
        res.redirect('/');
    });

        app.get('/auth/facebook', passport.authenticate('facebook', { scope : 'email',callbackURL:"/auth/facebook/callback/" }));

        // handle the callback after facebook has authenticated the user
        app.get('/auth/facebook/callback',
            passport.authenticate('facebook', {
            	callbackURL:"/auth/facebook/callback/",
                successRedirect : '/profile',
                failureRedirect : '/'
            }));

       

};